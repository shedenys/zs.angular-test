-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `grid`;
CREATE TABLE `grid` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `user` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `group` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `organization` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `grid` (`id`, `state`, `user`, `group`, `role`, `organization`, `created_at`, `updated_at`) VALUES
(1,	'1',	'user 1',	'Group 1',	'PM',	'ZS',	'2016-03-15 12:37:35',	'2016-03-15 12:37:35'),
(2,	'1',	'user 2',	'Group 2',	'PM',	'ZS',	'2016-03-15 12:37:35',	'2016-03-15 12:37:35'),
(3,	'0',	'user 3',	'Group 3',	'PM',	'ZS',	'2016-03-15 12:37:35',	'2016-03-15 12:37:35'),
(4,	'1',	'user 4',	'Group 4',	'PM',	'ZS',	'2016-03-15 12:37:35',	'2016-03-15 12:37:35'),
(5,	'1',	'user 5',	'Group 2',	'PM',	'ZS',	'2016-03-15 12:37:35',	'2016-03-15 12:37:35'),
(6,	'0',	'user 6',	'Group 3',	'PM',	'ZS',	'2016-03-15 12:37:35',	'2016-03-15 12:37:35'),
(7,	'0',	'user 7',	'Group 3',	'PM',	'ZS',	'2016-03-15 12:37:35',	'2016-03-15 12:37:35'),
(8,	'1',	'user 8',	'Group 3',	'PM',	'ZS',	'2016-03-15 12:37:35',	'2016-03-15 12:37:35'),
(9,	'1',	'user 9',	'Group 1',	'PM',	'ZS',	'2016-03-15 12:37:35',	'2016-03-15 12:37:35'),
(10,	'1',	'user 10',	'Group 1',	'PM',	'ZS',	'2016-03-15 12:37:35',	'2016-03-15 12:37:35'),
(11,	'1',	'user 11',	'Group 1',	'PM',	'ZS',	'2016-03-15 12:37:35',	'2016-03-15 12:37:35');

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table',	1),
('2014_10_12_100000_create_password_resets_table',	1),
('2016_03_15_134853_create_grids_table',	1);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- 2016-03-15 15:43:10
