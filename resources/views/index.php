<!doctype html>
<html lang="en" ng-app="gridApp">
<head>
	<meta charset="utf-8">
	<title>AngularJS. Тестове завдання</title>
	<link rel="stylesheet" href="/tpl/css/font-awesome.min.css"> <!-- load fontawesome -->
	<link rel="stylesheet" href="/tpl/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/tpl/css/style.css"/>
</head>
  
<body ng-controller="IndexController">
  
	<table class="table">
		<thead class="thead-inverse">
			<tr>
				<th><a href="#" ng-click="orderBy('id')" onclick="return(false)">ID <i class="{{ orderClass['id'].value }}"></i></a></th>
				<th><a href="#" ng-click="orderBy('state')" onclick="return(false)">State <i class="{{ orderClass['state'].value }}"></i></a></th>
				<th><a href="#" ng-click="orderBy('user')" onclick="return(false)">User <i class="{{ orderClass['user'].value }}"></i></a></th>
				<th><a href="#" ng-click="orderBy('group')" onclick="return(false)">Group <i class="{{ orderClass['group'].value }}"></i></a></th>
				<th><a href="#" ng-click="orderBy('role')" onclick="return(false)">Role <i class="{{ orderClass['role'].value }}"></i></a></th>
				<th><a href="#" ng-click="orderBy('organization')" onclick="return(false)">Organization <i class="{{ orderClass['organization'].value }}"></i></a></th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="item in items">
				<td>{{ item.id }}</td>
				<td>{{ item.state }}</td>
				<td>{{ item.user }}</td>
				<td>{{ item.group }}</td>
				<td>{{ item.role }}</td>
				<td>{{ item.organization }}</td>
			</tr>
			<tr>
				<td><input type="text" class="form-control" placeholder="ID" ng-change="filter()" ng-model="filterId"></td>
				<td><input type="text" class="form-control" placeholder="State" ng-change="filter()" ng-model="filterState"></td>
				<td><input type="text" class="form-control" placeholder="User" ng-change="filter()" ng-model="filterUser"></td>
				<td><input type="text" class="form-control" placeholder="Group" ng-change="filter()" ng-model="filterGroup"></td>
				<td><input type="text" class="form-control" placeholder="Role" ng-change="filter()" ng-model="filterRole"></td>
				<td><input type="text" class="form-control" placeholder="Organization" ng-change="filter()" ng-model="filterOrganization"></td>
			</tr>
		</tbody>
	</table>
	
	<div class="perPage">
		<span>Кількість записів на сторінці:</span>
		<select class="form-control" ng-options="item.title for item in perpage" ng-model="selected" ng-change="setPaginationPerPage()">
		</select>
	</div>

	<nav class="pagination">
	  <ul class="pagination">
	    <li>
	      <a href="#" aria-label="Previous" ng-click="setPaginationPage(prevPage)" onclick="return(false)">
	        <span aria-hidden="true">&laquo;</span>
	      </a>
	    </li>

	    <li ng-repeat="page in pages">
			<a href="#" ng-click="setPaginationPage(page)" onclick="return(false)">{{ page }}</a>
		</li>

	    <li>
	      <a href="#" aria-label="Next" ng-click="setPaginationPage(nextPage)" onclick="return(false)">
	        <span aria-hidden="true">&raquo;</span>
	      </a>
	    </li>
	  </ul>
	</nav>
  
	<script src="lib/angular/angular.min.js"></script>
	<script src="js/app.js"></script>
	<script src="js/controllers.js"></script>
	<script src="js/services.js"></script>

</body>
</html>