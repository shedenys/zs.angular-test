'use strict';

angular.module('gridApp.services', [])

	.factory('backendService', function($http) {
		return {
			get: function(orderTarget, orderDirection, perPage, pageNumber, filterData) {
				// return $http.get('api/items');
				return $http({
				    url: 'api/items', 
				    method: "GET",
				    params: {otarget: orderTarget, odirection: orderDirection, perpage: perPage, page: pageNumber, fdata: filterData }
				 });
			}
		}
	})

	.factory('paginationService', function() {

		return {
			pages: function(scope, lastPage) {
				scope.pages = [];
				var i = 0;
				while (i < lastPage) {
					scope.pages[i] = (i + 1);
					i++;
				}
			},
			nextPage: function(scope, currentPage, lastPage) {
				if(currentPage < lastPage) {
					scope.nextPage = currentPage + 1;
				}
				else {
					scope.nextPage = lastPage;
				}
			},
			prevPage: function(scope, currentPage) {
				if(currentPage > 1) {
					scope.prevPage = currentPage - 1;
				}
				else {
					scope.prevPage = 1;
				}
			},
			setPerPage: function(page) {
				localStorage.perPage = JSON.stringify(page);
			},
			getPerPage: function(scope) {
				var perPage;
				if(!localStorage.perPage) {
					localStorage.perPage = JSON.stringify(scope.perpage[0]);
				}
				return JSON.parse(localStorage.perPage);
			}

		}
	});